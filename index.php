<?php

error_reporting(-1);

$nullValue = null;
if (is_null($nullValue)){
    echo "null value check";
    echo "<br>";
}

$object = (object) array();
if (is_object($object)){
    echo "object value check";
    echo "<br>";
}

$string = "$nullValue Check String and null";
if (is_string($string)){
    echo 'string value check';
    echo '<br>';
}

$number = 123;
if (is_numeric($number)){
    echo 'yes it is number 1';
    echo "<br>";
}

$number = '123';
if (is_numeric($number)){
    if(!(is_int($number))){
        echo 'No it is not integer 2';
        echo'<br>';
    }
    echo 'yes it is number 2';
    echo "<br>";
} else {
    echo 'No it is not number 2';
    echo '<br>';
}

$number = 123.123;
if (is_numeric($number)) {
    if(is_float($number)){
        echo 'yes it is float';
        echo '<br>';
    }
    echo 'yes it is number 3';
    echo "<br>";
}

$bool = true;

echo ($bool ? 'true'  : 'false');
echo '<br>';

var_dump(gettype($string));
echo '<br>';
var_dump(gettype($number));
echo '<br>';
var_dump(gettype($bool));
echo '<br>';
var_dump(gettype($nullValue));
echo '<br>';
var_dump(gettype($object));
echo '<br>';

settype($nullValue, 'string');
var_dump($nullValue);
echo '<br>';

$variable = 10;

function localVariableCheck ()
{
    global $variable;
    echo $variable;
    echo '<br>';
}

localVariableCheck();

$numberOne = 100;
$numberTwo = 200;
$sum = 0;

function sum()
{
    global $numberOne, $numberTwo, $sum;
    $sum = $numberOne + $numberTwo;
}

sum();

echo $sum;
echo '<br>';

var_dump($_GET);
echo '<br>';
var_dump($_POST);
echo '<br>';
var_dump($_REQUEST);
echo '<br>';

const ARRAY_CONST = array(1,2,3,4,5);



?>

<form action="" method="post">
    <input type="text" name="Test">
    <input type ="submit">
</form>













