-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.38 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп данных таблицы personal_data.user_contacts: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_contacts` ENABLE KEYS */;

-- Дамп данных таблицы personal_data.user_details: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;

-- Дамп данных таблицы personal_data.user_education: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_education` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_education` ENABLE KEYS */;

-- Дамп данных таблицы personal_data.user_experience: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_experience` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_experience` ENABLE KEYS */;

-- Дамп данных таблицы personal_data.user_information: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `user_information` DISABLE KEYS */;
INSERT INTO `user_information` (`id`, `firstname`, `lastname`, `dob`, `vacancy`, `email`, `phone`, `adress`) VALUES
	(1, 'Andrii', 'Slobodian', '1995-11-12', 'Backend Developer', 'andriy_slobodian@gmail.com', '+380985784897', 'Ternopil, UA');
/*!40000 ALTER TABLE `user_information` ENABLE KEYS */;

-- Дамп данных таблицы personal_data.user_notes: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_notes` ENABLE KEYS */;

-- Дамп данных таблицы personal_data.user_skills: 5 rows
/*!40000 ALTER TABLE `user_skills` DISABLE KEYS */;
INSERT INTO `user_skills` (`id`, `user_id`, `skill`, `skill_category`, `percentage`) VALUES
	(1, 1, 'PHP', NULL, 90),
	(2, 1, 'JavaScript', NULL, 60),
	(3, 1, 'PHPStorm', NULL, 80),
	(4, 1, 'GIT', NULL, 75),
	(5, 1, 'MySQL', NULL, 70);
/*!40000 ALTER TABLE `user_skills` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
